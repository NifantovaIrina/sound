from http.server import HTTPServer, BaseHTTPRequestHandler
import urllib.parse as up

import  subprocess

HOST_NAME = "0.0.0.0"
PORT_NUMBER = 9000
statuses = None
id_max = 0


class Handler(BaseHTTPRequestHandler):
    f = None
    inp = None
    instr = "piano"

    def do_GET(self):
        global id_max
        print("here")
        self.send_response(200)
        self.send_header("Content-type", "audio")
        self.end_headers()
        if self.path.startswith("/status"):
            pars = up.urlparse(self.path)
            # self.wfile.write(statuses[pars['id']])
        elif self.path.startswith("/download"):
            self.wfile.write(self.inp)
        else:
            self.wfile.write(b"404")

    def do_POST(self):
        # self._set_headers()
        # global id_max
        # if self.path.startswith("/upload"):
        pars = up.urlparse(self.path)

        # print(self.path)
        # print(type(pars))
        # print(pars)
        # print(len(pars))

        self.instr = pars[4].split('=')[1]
        # print("instrument :", self.instr)
        # inp = self.rfile.read()
        # print(inp)
        #statuses[id_max] = "w"
        #id_max += 1
        input_fn = 'input.wav'
        output_fn = 'output.wav'
        print(self.path)
        with open("input", "w+b") as file:
            length = self.headers.get('Content-Length')
            length = int(length)
            self.inp = self.rfile.read(length)
            file.write(self.inp)
        subprocess.check_output(['ffmpeg', '-y', '-loglevel', 'panic', '-i', 'input', '-ac', '1', '-ar',
                                 '44100', 'input.wav'])
        subprocess.check_call(['/home/irina/anaconfa_test_do_not_del/bin/python3', 'analyzer.py'])
        subprocess.check_output(['ffmpeg', '-y', '-loglevel', 'panic', '-i', 'output.wav', 'output.mp3'])
        resp_data = open("output.mp3", 'rb').read()
        print(len(resp_data))
        self.send_response(200)
        self.send_header("Content-type", "audio")
        self.send_header('Content-Length', str(len(resp_data)))
        self.end_headers()

        self.wfile.write(resp_data)
        print("have written")


server_class = HTTPServer
httpd = server_class((HOST_NAME, PORT_NUMBER), Handler)
httpd.serve_forever()
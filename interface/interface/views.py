from django.http import HttpResponseRedirect
from django.shortcuts import render

from interface.forms import UploadFileForm


def main(request):
    return render(request, 'base.html')


def upload_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            # handle_uploaded_file(request.FILES['file'])
            return HttpResponseRedirect('download.html')
    else:
        form = UploadFileForm()
    return render(request, 'download.html', {'form': form})

def handle_uploaded_file(file):
    # здесь Мишин код, который кладет mp3 в некоторую папку
    pass